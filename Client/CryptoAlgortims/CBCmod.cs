﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoAlgortims
{
    public class CBCmod
    {
        public CBCmod() 
        {
            this.Key = "10010110";
        }
        public string Key
        {
            get { return this.key; }
            set { this.key = value;}
        }
        private string key;
        
        public char XOR(char a, char b)
        {
            if (a == b)
                return '0';
            else
                return '1';
        }

        public string Mod(string source)
        {
            char[] sourceEls = source.ToCharArray();
            char[] keyEls = this.key.ToCharArray();
            string outStr = "";
            int count = 0;
            foreach (char c in sourceEls)
            {
                char s = '0';
                s = XOR(c, keyEls[count]);
                outStr += s;
                count++;
            }
            return outStr;
        }
        public string Mod(string source, string prethodni)
        {
            char[] sourceEls  = source.ToCharArray();
            char[] keyEls = prethodni.ToCharArray();
            string outStr = "";
            int count = 0;
            foreach (char c in sourceEls)
            {
                char s = '0';
                s = XOR(c, keyEls[count]);
                outStr += s;
                count++;
            }
            return outStr;
        }
        public string DeMod(string source)
        {
            char[] sourceEls = source.ToCharArray();
            char[] keyEls = this.key.ToCharArray();
            string outStr = "";
            int count = 0;
            foreach (char c in sourceEls)
            {
                char s = '0';
                s = XOR(c, keyEls[count]);
                outStr += s;
                count++;
            }
            return outStr;
        }
        public string DeMod(string source, string prethodni)
        {
            char[] sourceEls = source.ToCharArray();
            char[] keyEls = prethodni.ToCharArray();
            string outStr = "";
            int count = 0;
            foreach (char c in sourceEls)
            {
                char s = '0';
                s = XOR(c, keyEls[count]);
                outStr += s;
                count++;
            }
            return outStr;
        }
    }
}

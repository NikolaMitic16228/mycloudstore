﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoAlgortims
{
    public class A51
    {
        public ShiftRegister X = new ShiftRegister();
        public ShiftRegister Y = new ShiftRegister();
        public ShiftRegister Z = new ShiftRegister();
        char[] novi = new char[64];
        public A51()
        {
            X.OperationMembers = new int[] { 13, 16, 17, 18 };
            Y.OperationMembers = new int[] { 20, 21 };
            Z.OperationMembers = new int[] { 7, 20, 21, 22 };
        }
        public A51(string sifra) : this()
        {
            this.Key = sifra;
        }
        public string Key
        {
            get { return this.key; }
            set
            {
                this.key = value;
                bool bits = false;
                foreach (char c in this.key.ToCharArray())
                {
                    if (c != '0')
                    {
                        if (c != '1')
                        {
                            bits = true;
                        }
                    }
                }
                char[] KeyEls;
                if(bits)
                {
                    string prevedeni = toBits(this.key);
                    if (prevedeni.Length < 64)
                    {
                        byte[] plainTextBytes;
                        plainTextBytes = Encoding.UTF8.GetBytes(prevedeni);
                        string noviNiz = Convert.ToBase64String(plainTextBytes);
                        KeyEls = toBits(noviNiz).ToCharArray();
                    }
                    else
                        KeyEls = prevedeni.ToCharArray();
                    Array.Copy(KeyEls, novi, novi.Length);
                    KeyEls = novi;
                }
                else
                {
                    if (this.key.Length < 64)
                    {
                        byte[] plainTextBytes;
                        plainTextBytes = Encoding.UTF8.GetBytes(this.key);
                        string noviNiz = Convert.ToBase64String(plainTextBytes);
                        KeyEls = toBits(noviNiz).ToCharArray();
                    }
                    else
                        KeyEls = this.key.ToCharArray();
                    Array.Copy(KeyEls, novi, novi.Length);
                    KeyEls = novi;
                }
                int i = 0;
                string pom = "";
                while (i < 19)
                {
                    pom += KeyEls[i];
                    i++;
                }
                X.Register = pom;
                pom = "";
                while (i < 41)
                {
                    pom += KeyEls[i];
                    i++;
                }
                Y.Register = pom;
                pom = "";
                while (i < 64)
                {
                    pom += KeyEls[i];
                    i++;
                }
                Z.Register = pom;
            }
        }
        private string key;
        public string toBits(string key)
        {
            byte[] btText;
            btText = Encoding.UTF8.GetBytes(key);
            Array.Reverse(btText);
            BitArray bit = new BitArray(btText);
            StringBuilder sb = new StringBuilder();
            for(int i = bit.Length-1;i>=0;i-- )
            {
                if (bit[i] == true)
                    sb.Append(1);
                else
                    sb.Append(0);
            }
            return sb.ToString();
        }
        public char XOR (char a, char b)
        {
            if (a == b)
                return '0';
            else
                return '1';
        }
        public void RegisterSteps(ShiftRegister sr)
        {
            char t = '0';
            foreach (int index in sr.OperationMembers)
                t = XOR(t, sr.Register[index]);
            sr.Shift(t);
        }
        public char MajorityVote()
        {
            int sum = 0;
            if (X.Register[8] == '1')
                sum++;
            if (Y.Register[11] == '1')
                sum++;
            if (Z.Register[14] == '1')
                sum++;
            if (sum > 1)
                return '1';
            else
                return '0';
        }
        public string Crypt(string source)
        {
           // bool bits = false;
            char[] sourceEls;
           /* foreach (char c in this.key.ToCharArray())
            {
                if (c != '0')
                {
                    if (c != '1')
                    {
                        bits = true;
                    }
                }
            }
            if (bits)
            {
                string preveden = toBits(source);
                sourceEls = preveden.ToCharArray();
            }
            else*/
                sourceEls = source.ToCharArray();
            
            string outStr = "";
            foreach(char c in sourceEls)
            {
                char m = MajorityVote();
                if (X.Register[8] == m)
                    RegisterSteps(X);
                if (Y.Register[11] == m)
                    RegisterSteps(Y);
                if (Z.Register[14] == m)
                    RegisterSteps(Z);
                char s = '0';
                s = XOR(XOR(XOR(s, X.Output), Y.Output), Z.Output);
                outStr += XOR(s, c);
            }
            return outStr;
        }
        public string DeCrypt(string source)
        {
            char[] sourceEls = source.ToCharArray();
            string outStr = "";
            foreach (char c in sourceEls)
            {
                char m = MajorityVote();
                if (X.Register[8] == m)
                    RegisterSteps(X);
                if (Y.Register[11] == m)
                    RegisterSteps(Y);
                if (Z.Register[14] == m)
                    RegisterSteps(Z);
                char s = '0';
                s = XOR(XOR(XOR(s, X.Output), Y.Output), Z.Output);
                outStr += XOR(s, c);
            }
            return outStr;
        }
    }
}

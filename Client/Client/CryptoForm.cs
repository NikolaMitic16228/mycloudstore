﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using CryptoAlgortims;

namespace Client
{
    public partial class CryptoForm : Form
    {
        string user;
        public CryptoForm(string username)
        {
            user = username;
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        String file;
        private void ChooseBtn_Click(object sender, EventArgs e)
        {
            int size = -1;
            openFileDialog2.InitialDirectory = @"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source";
            openFileDialog2.Title = "Odabir fajla za kriptovanje.";
            openFileDialog2.RestoreDirectory = true;
            DialogResult result = openFileDialog2.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                file = openFileDialog2.FileName;
                
                try
                {
                    fileNameLbl.Text = Path.GetFileName(file);
                    string text = File.ReadAllText(file);
                    size = text.Length;
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        private void DoWorkBtn_Click(object sender, EventArgs e)
        {
            string strChecked = null;
            foreach(Control c in groupBox1.Controls)
            {
                if(c.GetType() == typeof(RadioButton))
                {
                    RadioButton rb = c as RadioButton;
                    if(rb.Checked)
                    {
                        strChecked = rb.Text;
                    }
                }
            }
            if(fileNameLbl.Text == "")
            {
                MessageBox.Show("Niste izabrali nijedan fajl!", "Greska!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else if (strChecked == null)
            {
                MessageBox.Show("Niste izabrali nijedan algoritam!", "Greska!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else if(KeyTxtBox.Text == "")
            {
                MessageBox.Show("Niste uneli kljuc za kodiranje!", "Greska!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string hash = "1";
                string newContent = "";
                if(strChecked == "A5/1")
                {
                    byte[] bytcontent = File.ReadAllBytes(file);
                    string content = Encoding.ASCII.GetString(bytcontent.ToArray());

                    //string content = File.ReadAllText(file);
                    CBCmod cb = new CBCmod();
                    A51 a51 = new A51(KeyTxtBox.Text.Trim());
                    string binContent = a51.toBits(content);
                    string section = binContent.Substring(0, 8);
                    string partialContent = cb.Mod(section);
                    string partialnewContent = a51.Crypt(partialContent);
                    newContent += partialnewContent;
                    for (int i=8; i<binContent.Length; i+=8)
                    {
                        section = binContent.Substring(i, 8);
                        partialContent = cb.Mod(section,partialnewContent); // CBCmode
                        partialnewContent = a51.Crypt(partialContent); // A5/1
                        newContent += partialnewContent;
                    }

                    //ovo treba da bude zajednicko za sve
                    Proxy.Service1Client client = new Proxy.Service1Client();
                    string fileName = client.Upload(fileNameLbl.Text, newContent, hash, user);// File.Open(file,FileMode.Open));
                                                                                              //MessageBox.Show(fileName);
                                                                                              //System.IO.File.Move("F:\\Users\\Mitke\\Desktop\\ElFak\\4. godina\\7. semestar\\Zastita informacija\\Projekat\\mycloudstore\\Client\\WcfService\\Storage\\" + fileNameLbl.Text, "F:\\Users\\Mitke\\Desktop\\ElFak\\4. godina\\7. semestar\\Zastita informacija\\Projekat\\mycloudstore\\Client\\WcfService\\Storage\\" + user + "\\" + fileNameLbl.Text);
                    TextWriter tw = File.AppendText(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source\" + user + @"\SviFajlovi.txt");
                    tw.WriteLine(user + fileNameLbl.Text + " " + strChecked + " " + KeyTxtBox.Text);
                    tw.Close();
                }
                else if (strChecked == "RC6")
                {
                    MessageBox.Show("Aplikacija u razvoju...");
                }
                else if (strChecked == "RSA")
                {
                    MessageBox.Show("Aplikacija u razvoju...");
                }

                
                fileNameLbl.Text = null;
                KeyTxtBox.Clear();
                foreach (Control c in groupBox1.Controls)
                {
                    if (c.GetType() == typeof(RadioButton))
                    {
                        RadioButton rb = c as RadioButton;
                        if (rb.Checked)
                        {
                            rb.Checked = false;
                        }
                    }
                }
            }
        }

        private void showPassCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            KeyTxtBox.PasswordChar = showPassCheckBox.Checked ? '\0' : '*';
        }
    }
}

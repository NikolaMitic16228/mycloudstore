﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Client
{
    public partial class IndexForm : Form
    {
        String cs = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + "F:\\Users\\Mitke\\Desktop\\ElFak\\4. godina\\7. semestar\\Zastita informacija\\Projekat\\mycloudstore\\Client\\Client\\Login.mdf" + ";Integrated Security=True";
        public IndexForm()
        {
            InitializeComponent();
        }

        private void loginbtn_Click(object sender, EventArgs e)
        {
            if (usernameTxtBox.Text == "")
            {
                MessageBox.Show("Unesite vaše korisničko ime!", "Greška pri unosu podataka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (passwordTxtBox.Text == "")
            {
                MessageBox.Show("Unesite vašu lozinku!", "Greška pri unosu podataka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    SqlConnection myConnection = default(SqlConnection);
                    myConnection = new SqlConnection(cs);
                    SqlCommand myCommand = default(SqlCommand);
                    myCommand = new SqlCommand("SELECT Username,Password FROM Users WHERE Username=@Username AND Password=@Password", myConnection);
                    SqlParameter uName = new SqlParameter("@Username", SqlDbType.VarChar);
                    SqlParameter uPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                    uName.Value = usernameTxtBox.Text;
                    uPassword.Value = passwordTxtBox.Text;
                    myCommand.Parameters.Add(uName);
                    myCommand.Parameters.Add(uPassword);
                    myCommand.Connection.Open();
                    SqlDataReader myReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
                    if (myReader.Read() == true)
                    {
                        HomeForm hf = new HomeForm(usernameTxtBox.Text);
                        hf.Show();
                        this.Hide();
                        hf.FormClosing += Frm2_Closing;
                        usernameTxtBox.Clear();
                        passwordTxtBox.Clear();
                        showPassCheckBox.Checked = false;
                        
                    }
                    else
                    {
                        MessageBox.Show("Nepostojeći korisnik!", "Greška pri unosu podataka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        usernameTxtBox.Clear();
                        passwordTxtBox.Clear();
                        usernameTxtBox.Focus();
                        showPassCheckBox.Checked = false;
                    }
                    if (myConnection.State == ConnectionState.Open)
                    {
                        myConnection.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Greška!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void Frm2_Closing(object sender, FormClosingEventArgs e)
        {
            this.Show();
        }
        private void signupbtn_Click(object sender, EventArgs e)
        {
            usernameTxtBox.Clear();
            passwordTxtBox.Clear();
            showPassCheckBox.Checked = false;
            RegisterForm rf = new RegisterForm();
            rf.Show();
            this.Hide();
            rf.FormClosing += Frm2_Closing;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void passwordtxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void usernametxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void showPassCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            passwordTxtBox.PasswordChar = showPassCheckBox.Checked ? '\0' : '*';
        }
    }
}

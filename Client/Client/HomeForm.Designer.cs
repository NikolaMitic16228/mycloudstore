﻿namespace Client
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeForm));
            this.label1 = new System.Windows.Forms.Label();
            this.KriptujBtn = new System.Windows.Forms.Button();
            this.DekriptujBtn = new System.Windows.Forms.Button();
            this.CrytedFilesList = new System.Windows.Forms.ListView();
            this.NazivFajla = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LogOutBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Magneto", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(344, 45);
            this.label1.TabIndex = 1;
            this.label1.Text = "My Cloud Store";
            // 
            // KriptujBtn
            // 
            this.KriptujBtn.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KriptujBtn.Location = new System.Drawing.Point(20, 76);
            this.KriptujBtn.Name = "KriptujBtn";
            this.KriptujBtn.Size = new System.Drawing.Size(225, 29);
            this.KriptujBtn.TabIndex = 3;
            this.KriptujBtn.Text = "Kriptuj novi fajl";
            this.KriptujBtn.UseVisualStyleBackColor = true;
            this.KriptujBtn.Click += new System.EventHandler(this.KriptujBtn_Click);
            // 
            // DekriptujBtn
            // 
            this.DekriptujBtn.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DekriptujBtn.Location = new System.Drawing.Point(20, 111);
            this.DekriptujBtn.Name = "DekriptujBtn";
            this.DekriptujBtn.Size = new System.Drawing.Size(225, 29);
            this.DekriptujBtn.TabIndex = 4;
            this.DekriptujBtn.Text = "Dekriptuj sa Cloud-a";
            this.DekriptujBtn.UseVisualStyleBackColor = true;
            this.DekriptujBtn.Click += new System.EventHandler(this.DekriptujBtn_Click);
            // 
            // CrytedFilesList
            // 
            this.CrytedFilesList.CheckBoxes = true;
            this.CrytedFilesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NazivFajla});
            this.CrytedFilesList.HideSelection = false;
            this.CrytedFilesList.Location = new System.Drawing.Point(20, 146);
            this.CrytedFilesList.MultiSelect = false;
            this.CrytedFilesList.Name = "CrytedFilesList";
            this.CrytedFilesList.Size = new System.Drawing.Size(316, 292);
            this.CrytedFilesList.TabIndex = 5;
            this.CrytedFilesList.UseCompatibleStateImageBehavior = false;
            this.CrytedFilesList.View = System.Windows.Forms.View.Details;
            // 
            // NazivFajla
            // 
            this.NazivFajla.Text = "Naziv fajla";
            this.NazivFajla.Width = 312;
            // 
            // LogOutBtn
            // 
            this.LogOutBtn.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogOutBtn.Location = new System.Drawing.Point(500, 12);
            this.LogOutBtn.Name = "LogOutBtn";
            this.LogOutBtn.Size = new System.Drawing.Size(106, 30);
            this.LogOutBtn.TabIndex = 6;
            this.LogOutBtn.Text = "Odjavi se";
            this.LogOutBtn.UseVisualStyleBackColor = true;
            this.LogOutBtn.Click += new System.EventHandler(this.LogOutBtn_Click);
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(618, 450);
            this.Controls.Add(this.LogOutBtn);
            this.Controls.Add(this.CrytedFilesList);
            this.Controls.Add(this.DekriptujBtn);
            this.Controls.Add(this.KriptujBtn);
            this.Controls.Add(this.label1);
            this.Name = "HomeForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button KriptujBtn;
        private System.Windows.Forms.Button DekriptujBtn;
        private System.Windows.Forms.ListView CrytedFilesList;
        private System.Windows.Forms.ColumnHeader NazivFajla;
        private System.Windows.Forms.Button LogOutBtn;
    }
}
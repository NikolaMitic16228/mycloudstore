﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using CryptoAlgortims;

namespace Client
{
    public partial class HomeForm : Form
    {

        ListViewItem lvi;
        string user;
        List<string> filesIn = new List<string>();
        List<byte> moji = new List<byte>();
        public HomeForm(string username)
        {
            InitializeComponent();
            user = username;
            if (!System.IO.Directory.Exists(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source\" + user))
            {
                System.IO.Directory.CreateDirectory(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source\" + user);
                System.IO.Directory.CreateDirectory(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source\" + user + @"\Downloaded");
                System.IO.File.Create(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source\" + user + @"\SviFajlovi.txt");
            }
            string[] files = Directory.GetFiles(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\WcfService\Storage", user+"*.*");
            foreach (string s in files)
            {
                    lvi = new ListViewItem(Path.GetFileName(s));
                    CrytedFilesList.Items.Add(lvi);
            }
        }

        private void KriptujBtn_Click(object sender, EventArgs e)
        {
            CryptoForm cf = new CryptoForm(user);
            cf.Show();
            this.Hide();
            cf.FormClosing += Frm3_Closing;
        }
        private void Frm3_Closing(object sender, FormClosingEventArgs e)
        {
            this.Show();
            CrytedFilesList.Items.Clear();
            string[] files = Directory.GetFiles(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\WcfService\Storage", user + "*.*");
            foreach (string s in files)
            {
                    lvi = new ListViewItem(Path.GetFileName(s));
                    CrytedFilesList.Items.Add(lvi);
            }
        }


        private void DekriptujBtn_Click(object sender, EventArgs e)
        {
            bool nestoCekirano = false;
            string nestoCekiranoIme = "";
            foreach (ListViewItem item in CrytedFilesList.Items)
            {
                if (item.Checked)
                {
                    nestoCekirano = true;
                    nestoCekiranoIme = item.Text;
                }
            }
            if (nestoCekirano)
            {
                Proxy.Service1Client client = new Proxy.Service1Client();
                var stream = client.Download(nestoCekiranoIme);// CrytedFilesList.SelectedItems[0].Text);
                string hash = "";
                string contet = "";
                StreamReader reader = new StreamReader(stream);
                string text = reader.ReadToEnd();
                reader.Close();
                hash = text.Substring(0, text.IndexOf("\n"));
                contet = text.Substring(text.IndexOf("\n") + 1, text.Length - text.IndexOf("\n") - 1);

                string name = "";
                string algType = "";
                string key = "";
                string line = "";
                int counter = 0;
                reader = new StreamReader(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source\" + user + @"\SviFajlovi.txt");
                while ((line = reader.ReadLine()) != null)
                {
                    filesIn.Add(line);
                    counter++;
                }
                reader.Close();

                for (int i = 0; i < counter; i++)
                {
                    string[] tokens = filesIn[i].Split(' ');
                    if (tokens[0] == nestoCekiranoIme) //CrytedFilesList.SelectedItems[0].Text)
                    {
                        name = tokens[0];
                        algType = tokens[1];
                        key = tokens[2];
                    }

                }
                string decripted = "";
                if (algType == "A5/1")
                {
                    CBCmod cb = new CBCmod();
                    A51 a51 = new A51(key);
                    string section = contet.Substring(0, 8);
                    string sectionCpy = section;
                    string partialContent = a51.DeCrypt(section);
                    string partialnewContent = cb.DeMod(partialContent);
                    decripted += partialnewContent;
                    for (int i = 8; i < contet.Length; i += 8)
                    {
                        section = contet.Substring(i, 8);
                        partialContent = a51.DeCrypt(section); // A5/1
                        partialnewContent = cb.DeMod(partialContent, sectionCpy); // CBCmode
                        decripted += partialnewContent;
                        sectionCpy = section;
                    }


                    //staro
                    //A51 a51 = new A51(key);
                    // decripted = a51.DeCrypt(contet);
                }
                else if (algType == "RC6")
                {

                }
                else if (algType == "RSA")
                {

                }
                List<Byte> byteList = new List<Byte>();

                for (int i = 0; i < decripted.Length; i += 8)
                {
                    byteList.Add(Convert.ToByte(decripted.Substring(i, 8), 2));
                }
                string newContent = Encoding.ASCII.GetString(byteList.ToArray());
                string fileName = @"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source\" + user + @"\Downloaded\" + name;
                FileInfo fi = new FileInfo(fileName);
                using (FileStream fs = fi.Create())
                {
                    Byte[] txt = new UTF8Encoding(true).GetBytes(newContent);
                    fs.Write(txt, 0, txt.Length);
                }

                System.Diagnostics.Process.Start(@"F:\Users\Mitke\Desktop\ElFak\4. godina\7. semestar\Zastita informacija\Projekat\mycloudstore\Client\Client\Source\" + user + @"\Downloaded\" + name);
            }
            else
                MessageBox.Show("Nista nije odabrano za Download.");
        }

        private void LogOutBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

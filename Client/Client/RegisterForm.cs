﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Client
{
    public partial class RegisterForm : Form
    {
        String cs = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename="+"F:\\Users\\Mitke\\Desktop\\ElFak\\4. godina\\7. semestar\\Zastita informacija\\Projekat\\mycloudstore\\Client\\Client\\Login.mdf"+";Integrated Security=True";
        
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            usernameTxtBox.Clear();
            passwordTxtBox.Clear();
            showPassCheckBox.Checked = false;
            this.Close();
        }

        private void RegisterBtn_Click(object sender, EventArgs e)
        {
            if (usernameTxtBox.Text == "")
            {
                MessageBox.Show("Unesite vaše korisničko ime!", "Greška pri unosu podataka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (passwordTxtBox.Text == "")
            {
                MessageBox.Show("Unesite vašu lozinku!", "Greška pri unosu podataka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                
                try
                {
                    SqlConnection myConnection = new SqlConnection(cs);
                    myConnection.Open();
                    SqlCommand myCommand = new SqlCommand("INSERT INTO Users (Username, Password) VALUES(@Username, @Password)", myConnection);
                    SqlParameter uName = new SqlParameter("@Username", SqlDbType.VarChar);
                    SqlParameter uPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                    uName.Value = usernameTxtBox.Text;
                    uPassword.Value = passwordTxtBox.Text;
                    myCommand.Parameters.Add(uName);
                    myCommand.Parameters.Add(uPassword);
                    int result = myCommand.ExecuteNonQuery();
                    if (result< 0)
                    {
                        MessageBox.Show("Registrovanje neuspešno!", "Greška pri unosu podataka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        usernameTxtBox.Clear();
                        passwordTxtBox.Clear();
                        usernameTxtBox.Focus();
                        showPassCheckBox.Checked = false;
                    }
                    else
                    {
                        MessageBox.Show("Registrovanje uspešno!", "Preusmeravanje...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    
                    if (myConnection.State == ConnectionState.Open)
                    {
                        myConnection.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Greška!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void showPassCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            passwordTxtBox.PasswordChar = showPassCheckBox.Checked ? '\0' : '*';
        }
    }
}

﻿namespace Client
{
    partial class CryptoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CryptoForm));
            this.BackBtn = new System.Windows.Forms.Button();
            this.A51RBtn = new System.Windows.Forms.RadioButton();
            this.RC6RBtn = new System.Windows.Forms.RadioButton();
            this.RSARBtn = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TitleLbl = new System.Windows.Forms.Label();
            this.DoWorkBtn = new System.Windows.Forms.Button();
            this.ChooseBtn = new System.Windows.Forms.Button();
            this.fileNameLbl = new System.Windows.Forms.Label();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.SifraLbl = new System.Windows.Forms.Label();
            this.KeyTxtBox = new System.Windows.Forms.TextBox();
            this.showPassCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BackBtn
            // 
            this.BackBtn.BackColor = System.Drawing.Color.Transparent;
            this.BackBtn.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackBtn.Location = new System.Drawing.Point(500, 12);
            this.BackBtn.Name = "BackBtn";
            this.BackBtn.Size = new System.Drawing.Size(108, 32);
            this.BackBtn.TabIndex = 0;
            this.BackBtn.Text = "Nazad";
            this.BackBtn.UseVisualStyleBackColor = false;
            this.BackBtn.Click += new System.EventHandler(this.BackBtn_Click);
            // 
            // A51RBtn
            // 
            this.A51RBtn.AutoSize = true;
            this.A51RBtn.Location = new System.Drawing.Point(6, 13);
            this.A51RBtn.Name = "A51RBtn";
            this.A51RBtn.Size = new System.Drawing.Size(71, 24);
            this.A51RBtn.TabIndex = 1;
            this.A51RBtn.TabStop = true;
            this.A51RBtn.Text = "A5/1";
            this.A51RBtn.UseVisualStyleBackColor = true;
            this.A51RBtn.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // RC6RBtn
            // 
            this.RC6RBtn.AutoSize = true;
            this.RC6RBtn.Location = new System.Drawing.Point(6, 43);
            this.RC6RBtn.Name = "RC6RBtn";
            this.RC6RBtn.Size = new System.Drawing.Size(65, 24);
            this.RC6RBtn.TabIndex = 2;
            this.RC6RBtn.TabStop = true;
            this.RC6RBtn.Text = "RC6";
            this.RC6RBtn.UseVisualStyleBackColor = true;
            // 
            // RSARBtn
            // 
            this.RSARBtn.AutoSize = true;
            this.RSARBtn.Location = new System.Drawing.Point(6, 73);
            this.RSARBtn.Name = "RSARBtn";
            this.RSARBtn.Size = new System.Drawing.Size(74, 24);
            this.RSARBtn.TabIndex = 4;
            this.RSARBtn.TabStop = true;
            this.RSARBtn.Text = "RSA";
            this.RSARBtn.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.RSARBtn);
            this.groupBox1.Controls.Add(this.RC6RBtn);
            this.groupBox1.Controls.Add(this.A51RBtn);
            this.groupBox1.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 153);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 103);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kripto algoritmi";
            // 
            // TitleLbl
            // 
            this.TitleLbl.AutoSize = true;
            this.TitleLbl.BackColor = System.Drawing.Color.Transparent;
            this.TitleLbl.Font = new System.Drawing.Font("Magneto", 27.75F, System.Drawing.FontStyle.Bold);
            this.TitleLbl.Location = new System.Drawing.Point(12, 9);
            this.TitleLbl.Name = "TitleLbl";
            this.TitleLbl.Size = new System.Drawing.Size(344, 45);
            this.TitleLbl.TabIndex = 5;
            this.TitleLbl.Text = "My Cloud Store";
            // 
            // DoWorkBtn
            // 
            this.DoWorkBtn.BackColor = System.Drawing.Color.Transparent;
            this.DoWorkBtn.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DoWorkBtn.Location = new System.Drawing.Point(12, 317);
            this.DoWorkBtn.Name = "DoWorkBtn";
            this.DoWorkBtn.Size = new System.Drawing.Size(290, 38);
            this.DoWorkBtn.TabIndex = 8;
            this.DoWorkBtn.Text = "Kriptuj";
            this.DoWorkBtn.UseVisualStyleBackColor = false;
            this.DoWorkBtn.Click += new System.EventHandler(this.DoWorkBtn_Click);
            // 
            // ChooseBtn
            // 
            this.ChooseBtn.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChooseBtn.Location = new System.Drawing.Point(13, 81);
            this.ChooseBtn.Name = "ChooseBtn";
            this.ChooseBtn.Size = new System.Drawing.Size(200, 40);
            this.ChooseBtn.TabIndex = 9;
            this.ChooseBtn.Text = "Fajl za kriptovanje";
            this.ChooseBtn.UseVisualStyleBackColor = true;
            this.ChooseBtn.Click += new System.EventHandler(this.ChooseBtn_Click);
            // 
            // fileNameLbl
            // 
            this.fileNameLbl.AutoSize = true;
            this.fileNameLbl.BackColor = System.Drawing.Color.Transparent;
            this.fileNameLbl.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileNameLbl.Location = new System.Drawing.Point(15, 124);
            this.fileNameLbl.Name = "fileNameLbl";
            this.fileNameLbl.Size = new System.Drawing.Size(0, 20);
            this.fileNameLbl.TabIndex = 10;
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // SifraLbl
            // 
            this.SifraLbl.AutoSize = true;
            this.SifraLbl.BackColor = System.Drawing.Color.Transparent;
            this.SifraLbl.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SifraLbl.Location = new System.Drawing.Point(12, 283);
            this.SifraLbl.Name = "SifraLbl";
            this.SifraLbl.Size = new System.Drawing.Size(57, 20);
            this.SifraLbl.TabIndex = 11;
            this.SifraLbl.Text = "Šifra";
            // 
            // KeyTxtBox
            // 
            this.KeyTxtBox.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyTxtBox.Location = new System.Drawing.Point(75, 280);
            this.KeyTxtBox.Name = "KeyTxtBox";
            this.KeyTxtBox.PasswordChar = '*';
            this.KeyTxtBox.Size = new System.Drawing.Size(138, 27);
            this.KeyTxtBox.TabIndex = 12;
            // 
            // showPassCheckBox
            // 
            this.showPassCheckBox.AutoSize = true;
            this.showPassCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.showPassCheckBox.Font = new System.Drawing.Font("Magneto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showPassCheckBox.Location = new System.Drawing.Point(219, 282);
            this.showPassCheckBox.Name = "showPassCheckBox";
            this.showPassCheckBox.Size = new System.Drawing.Size(139, 24);
            this.showPassCheckBox.TabIndex = 13;
            this.showPassCheckBox.Text = "Prikaži šifru";
            this.showPassCheckBox.UseVisualStyleBackColor = false;
            this.showPassCheckBox.CheckedChanged += new System.EventHandler(this.showPassCheckBox_CheckedChanged);
            // 
            // CryptoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(620, 391);
            this.Controls.Add(this.showPassCheckBox);
            this.Controls.Add(this.KeyTxtBox);
            this.Controls.Add(this.SifraLbl);
            this.Controls.Add(this.fileNameLbl);
            this.Controls.Add(this.ChooseBtn);
            this.Controls.Add(this.DoWorkBtn);
            this.Controls.Add(this.TitleLbl);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BackBtn);
            this.Name = "CryptoForm";
            this.Text = "Kriptujte svoje fajlove...";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BackBtn;
        private System.Windows.Forms.RadioButton A51RBtn;
        private System.Windows.Forms.RadioButton RC6RBtn;
        private System.Windows.Forms.RadioButton RSARBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label TitleLbl;
        private System.Windows.Forms.Button DoWorkBtn;
        private System.Windows.Forms.Button ChooseBtn;
        private System.Windows.Forms.Label fileNameLbl;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.Label SifraLbl;
        private System.Windows.Forms.TextBox KeyTxtBox;
        private System.Windows.Forms.CheckBox showPassCheckBox;
    }
}